require('dotenv').config();

// Nos traemos el Framework
const express = require('express');

// Por convenio, la constante se llama 'app'
const app = express();

//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json


// Importamos las funciones exportadas que se hayan indicado en nuestro fichero AuthController.js
const authController = require('./js/controllers/AuthController');
// Importamos las funciones exportadas que se hayan indicado en nuestro fichero ClientController.js
const clientController = require('./js/controllers/ClientController');
// Importamos las funciones exportadas que se hayan indicado en nuestro fichero AccountController.js
const accountController = require('./js/controllers/AccountController');
// Importamos las funciones exportadas que se hayan indicado en nuestro fichero MovementController.js
const movementController = require('./js/controllers/MovementController');

//manejador para poder hacer peticiones desde cualquier origen. lo metemos mas abajo como manejador.
var enableCORS = function (req, res, next){
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
res.set("Access-Control-Allow-Headers", "Content-Type");

next();
}

// le pasamos otra capa de middleware. Hay una política que impide que se hagan peticiones desde una url a otra. Esto permite que suceda estas llamadas
app.use(enableCORS);

// Con esto pasamos una capa middleware al framework para que pueda parsear los parámetros enviados en el BODY
app.use(express.json());

/* Establecemos el puerto en el que escuchará nuestro proyecto.
 * Si existe la variable de entorno PORT, la recuperamos.
 * Sino, estableceremos el puerto directamente
 */
const port = process.env.PORT || 3000;

app.listen(port);
console.log("API escuchando en el puerto " + port);

/***** LOCAL ****************/
// Autenticación de un cliente
app.post('/osbank/local/login', authController.loginClientLocal);
// Logout del Cliente
app.post('/osbank/local/logout/:id', authController.logoutClientLocal);
// Lista de clientes
app.get('/osbank/local/clients', clientController.getClientsLocal);
// Añadir un nuevo usuario con password
app.post('/osbank/local/clients', clientController.createClientLocal);
// Eliminar el usuario a partir de su Id
app.delete('/osbank/local/clients/:id', clientController.deleteClientLocal);


/***** MLAB ****************/
// Autenticación de un cliente
app.post('/osbank/mlab/login', authController.loginClient);
// Logout del Cliente
app.post('/osbank/mlab/logout/:id', authController.logoutClient);
// Lista de clientes
app.get('/osbank/mlab/clients', clientController.getClients);
// Obtener usuario en base a su Id
app.get('/osbank/mlab/clients/:id', clientController.getClientById);
// Añadir un nuevo usuario con password
app.post('/osbank/mlab/clients', clientController.createClient);
// Eliminar el usuario a partir de su Id
app.delete('/osbank/mlab/clients/:id', clientController.deleteClient);
// Consulta las cuentas para un Cliente concreto
app.get('/osbank/mlab/accounts/:id', accountController.getAccountsById);
// Dar de alta un movimiento a partir de una cuenta
app.post('/osbank/mlab/movements', movementController.createMovement);
// Consultar los movimientos de una cuenta concreta
app.get('/osbank/mlab/movements/:iban', movementController.getMovementsByIban);
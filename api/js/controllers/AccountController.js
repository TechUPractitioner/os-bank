// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-jason'
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";


//const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"


function getAccountsById(req, res) {
 
  console.log("GET /osbank/mlab/accounts");

 var id = req.params.id;
 console.log("Id del Cliente del que queremos consultar las cuentas " + id);
 var query = 'q={"userId":' + id + '}';
 console.log("query es " + query);

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");
 
 //httpClient.get("accounts?" + query + "&" + mLabAPIKey,
 httpClient.get("accounts?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
    console.log("BODY " + body);
    console.log(body);

    
     if (err) {
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body;
       } else {
         var response = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404);
       }
     }

     res.send(response);
   }
 );
}

module.exports.getAccountsById = getAccountsById;

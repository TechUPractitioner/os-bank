// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-jason'
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";


//const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"


function getMovementsByIban(req, res) {

  console.log("MovementControllers.getMovementsByIban");
 
  console.log("GET osbank/mlab/movements/" + req.params.iban);

 var iban = req.params.iban;
 console.log("Iban de la cuenta de la que queremos consultar los movimientos" + iban);
 var query = 'q={"iban":' + iban + '}';
 console.log("query es " + query);

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");
 
  httpClient.get("movements?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
    console.log("resMLab " + JSON.stringify(resMLab));
    console.log("BODY " + body);
    console.log(body);

    
     if (err) {
       var response = {
         "msg" : "Error obteniendo los movimientos"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body;
       } else {
         var response = {
           "msg" : "iban no encontrado"
         }
         res.status(404);
       }
     }

     res.send(response);
   }
 );
}


// Función para dar de alta un movimiento asociado una cuenta determinada, ejecutando en el Server Node JS
function createMovement(req,res) {

  console.log("MovementControllers.createMovement");
  console.log("POST /osbank/mlab/movements");

  console.log("iban es " + req.body.iban);
  console.log("tipo de movimiento es " + req.body.type);
  console.log("importe es " + req.body.amount);
  console.log("el concepto es: " + req.body.concept)

  // creamos el Object newMovement
  var newMovement = {
    "iban" : req.body.account,
    "description" : req.body.description,
    "amount" : req.body.amount,
    "type" : req.body.type
  };

  // movimientos es un tipo array de Js. Para añadir un elemento, usamos la función push
  var movimientos = require('../../json/movimientos.json');
  movimientos.push(newMovement);
  io.writeMovementToFile(movimientos);
  console.log("Movimiento añadido");
  res.status(201).send({"msg" : "Movimiento dado de alta correctamente"})
}

module.exports.getMovementsByIban = getMovementsByIban;
module.exports.createMovement = createMovement;

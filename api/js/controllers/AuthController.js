// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-json' para gestionar el Cliente Http
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";

//const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"

function loginClientLocal(req,res) {
  console.log("POST /osbank/local/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var clients = require('../../json/clients.json');
  var encontrado = false;
  var idUsuario;

   for (var i = 0; i < clients.length; i++) {
     console.log("comparando " + clients[i].email + " y " +  req.body.email);
     if ((clients[i].email == req.body.email) && (crypt.checkPassword(req.body.password, clients[i].password))){
       console.log("Usuario y contraseña coinciden");
       clients[i].logged = true;
       encontrado = true;
       idUsuario = clients[i].id;
       break;

     }
   }

   if (encontrado) {
     io.writeUserDataToFile(clients);
     //writeUserDataToFile(users);
   }

   var msg = encontrado ? "Login correcto " : "Login incorrecto";


   // Esta es la manera en la que el profesor implementa el mensaje de respuesta, previo al res.send(mensaje)
   var response = {
     "mensaje" : msg,
     "idUsuario" : idUsuario   // Si idUsuario es undefined, la propiedad idUsuario no se crea
   }

   console.log(msg);
   res.send(response);
}

// Función para deslogar a un cliente en el entorno del Server Node JS
function logoutClientLocal(req,res) {
  console.log("POST /osbank/local/logout/:id");

  console.log("id es " + req.params.id);


  var clients = require('../../json/clients.json');
  var encontrado = false;
  var idUsuario;

   for (var i = 0; i < clients.length; i++) {
     console.log("comparando " + clients[i].id + " y " +  req.params.id);
     if (clients[i].id == req.params.id && clients[i].logged === true){
       console.log("El Cliente hace Logout");
       delete clients[i].logged;
       encontrado = true;
       idUsuario = clients[i].id;
       break;
     }
   }

   if (encontrado) {
     io.writeUserDataToFile(clients);
   }


   var msg = encontrado ? "Logout correcto , idUsuario : " +  idUsuario : "Logout incorrecto";

   console.log(msg);
   res.send({"mensaje" : msg});
}


// Login de un Cliente accediendo al Backend en MLAB
function loginClient(req, res) {
  console.log("POST /osbank/mlab/login");
  console.log("mLabAPIKey = " + mLabAPIKey);  

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  console.log("req.body = " + JSON.stringify(req.body));
  var autenticado = false;
  console.log("req.body.email = " + req.body.email);
  var email = req.body.email;
  // La variable "id" tendrá el id del cliente en caso de que el Login sea correcto.
  var id = "";
  var query = 'q={"email": "' + email + '"}';
  console.log("query = " + query);

  httpClient.get("clients?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      // muestra el array body por consola, para poder ver qué valores contiene
      console.log(JSON.stringify(body));

      if (err) {
        var response = {
          "msg" : "Error obteniendo Cliente"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("Vamos a validar si la passsword enviada en el Body coincide con la hasheada que está almacenada en MLAB");

          if (crypt.checkPassword(req.body.password, body[0].password)){
           console.log("La password es correcta");
           autenticado = true;
           id = body[0].id;
           // Tenemos que añadir un campo que indique que el usuario está logado.
           var putBody = '{"$set" : {"logged" : true}}';
           var query2 = 'q={"id": ' + id + '}';


           httpClient.put("clients?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
                console.log("estamos dentro del segundo httpCliente");
            }
           )

           var response = body[0];
           console.log("response = " + response);

          } else {

            console.log("Las credenciales no coinciden. Usuario no autorizado");
            res.status(401);
          }
        } else {
          var response = {
            "msg" : "Cliente no encontrado"
          }
          res.status(404);
        }
      }

      var msg = autenticado ?
        "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";



      var response = {
          "mensaje": msg,
          "id": id,
          "usuario": email,
          "isLogged": autenticado
        };


      res.send(response);
    }
  )

}




/**********************

function loginUserV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var autenticado = false;
  var email = req.body.email;
  var query = 'q={"email": "' + email + '"}';
  console.log("query = " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log("body.length = " + body.length);
      console.log("body[0] = " + body[0]);
      console.log(JSON.stringify(body));

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("Vamos a validar si la passsword enviada en el Body coincide con la hasheada que está almacenada en MLAB");

          if (crypt.checkPassword(req.body.password, body[0].password)){
           console.log("La password es correcta");
           autenticado = true;
           // Tenemos que añadir un campo que indique que el usuario está logado.
           var putBody = '{"$set" : {"logged" : true}}';
           var query2 = 'q={"id": ' + body[0].id + '}';

           httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
                console.log("estamos dentro del segundo httpCliente");
            }
           )

           var response = body[0];

          } else {

            console.log("Las credenciales no coinciden. Usuario no autorizado");
            res.status(401);
          }
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }

      var msg = autenticado ?
        "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

      var response = {
          "mensaje": msg,
          "usuario": email
        };

      res.send(response);
    }
  )

}




function logoutUserV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 var query = 'q={"id": ' + id + '}';
 console.log("query = " + query);
 var msg = "Logout Incorrecto";
 var response = "";

 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     console.log("Entramos en httpClient");

     // Si el id está registrado en la BBDD
     if (body.length > 0) {
       var putBody = '{"$unset" : {"logged" : ""}}';

       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
        function(errPUT, resMLabPUT, bodyPUT) {
            console.log("estamos dentro del segundo httpCliente");

            msg = "Logout correcto";

            response = {
                "mensaje": msg,
                "idUsuario": ""
            };

            res.send(response);
        }
       )
     }

     response = {
         "mensaje": msg,
         "idUsuario": id
     };

     res.send(response);


   }
 )


}

**********/

// Login de Carlos V2
function loginUserV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

// // Logout de Carlos V2
function logoutClient(req, res) {
 console.log("POST /osbank/mlab/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("clients?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("El usuario existe y quiere cerrar la sesión");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("La consulta es " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("clients?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT realizado");
           var response = {
             "msg" : "El Cliente ha cerrado su sesión",
             "idCliente" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginClientLocal = loginClientLocal;
module.exports.logoutClientLocal = logoutClientLocal;

module.exports.loginClient = loginClient;
module.exports.logoutClient = logoutClient;

// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-json' para gestionar el Cliente Http
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMlabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


// Función para obtener el listado de clientes
function getClientsLocal(req, res) {
  console.log("GET /osbank/local/clients");

  var result = {};
  var clients = require('../../json/clients.json');

  console.log(req.query);

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = clients.length;
  }

  result.clients = req.query.$top ? clients.slice(0, req.query.$top) : clients;
  res.send(result);
}


// Listado de Clientes en el Backend de Mongo DB
function getClients(req, res) {
  console.log("GET /osbank/mlab/clients");
  console.log("Entramos en getClients");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

// resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
  httpClient.get("clients?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo clientes"
      }

      res.send(response);
    }
  )

}

// Obtención de un cliente por su Id
function getClientById(req, res) {
 console.log("GET /osbank/mlab/clients/:id");

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del Cliente a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get("clients?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     if (err) {
       var response = {
         "msg" : "Error obteniendo cliente"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body[0];
       } else {
         var response = {
           "msg" : "Cliente no encontrado"
         }
         res.status(404);
       }
     }

     res.send(response);
   }
 )
}

// Función para dar de alta un cliente en el Server Node JS
function createClientLocal(req,res) {
  console.log("POST /osbank/local/clients");

  console.log("id es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("password es " + req.body.password);

  // creamos el Object newUser
  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)

  };

  // users es un tipo array de Js. Para añadir un elemento, usamos la función push
  var users = require('../../json/clients.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("Usuario añadido");
  res.status(201).send({"msg" : "Cliente dado de alta correctamente"})
}

// Función para dar de alta un cliente utilizando como Backend Mongo DB
function createClient(req,res) {
  console.log("POST /osbank/mlab/clients");

  console.log("id es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("password es " + req.body.password);

  var newClient = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("clients?" + mLabAPIKey, newClient,
    function(err, resMLab, body) {
      console.log("Cliente dado de alta correctamente");
      // status 201 significa "creado"
      res.status(201).send({"msg" : "Cliente dado de alta correctamente"})
    }
  )

}


// Función para dar de baja un cliente en el Server Node JS
function deleteClientLocal(req, res) {
  console.log("DELETE /osbank/local/clients/:id");
  console.log("La id del cliente a dar de baja es " + req.params.id);

  var clients = require('../../json/clients.json');
  var deleted = false;

  for (var i = 0; i < clients.length; i++) {
    console.log("comparando " + clients[i].id + " y " +  req.params.id);
    if (clients[i].id == req.params.id) {
       console.log("La posicion " + i + " coincide");
       clients.splice(i, 1);
       deleted = true;
       break;
     }
   }

  if (deleted) {
    io.writeUserDataToFile(clients);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}


// Función para dar de baja un cliente en el Backend de MLAB
function deleteClient(req, res) {
  console.log("DELETE /osbank/mlab/clients/:id");
  console.log("id es " + req.params.id);

  var clients = require('../../json/clients.json');
  var deleted = false;

  for (var i = 0; i < clients.length; i++) {
    console.log("comparando " + clients[i].id + " y " +  req.params.id);
    if (clients[i].id == req.params.id) {
       console.log("La posicion " + i + " coincide");
       clients.splice(i, 1);
       deleted = true;
       break;
     }
   }

  if (deleted) {
    io.writeUserDataToFile(clients);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}




/***** LOCAL ****************/
module.exports.createClientLocal = createClientLocal;
module.exports.getClientsLocal = getClientsLocal;
module.exports.deleteClientLocal = deleteClientLocal;

/***** LOCAL ****************/
module.exports.createClient = createClient;
module.exports.getClients = getClients;
module.exports.getClientById = getClientById;
module.exports.deleteClient = deleteClient;
